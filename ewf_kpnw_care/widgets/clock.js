/**
 * Menu this
 * Menu is a list of items (usually <a>) that user can use either keyboard
 * to scroll, navigate and activate.
 */
var Ion = Ion || {};
Ion.Widget = Ion.Widget || {};

(function ($) {
    
    Ion.Widget.Clock = Backbone.View.extend({
        // common Widget properties
        el: 'div#clock',  //eg: mainmenu
        
        // Menu properties
        format: 'ddd mmm dd yyyy h:MM TT',
        interval: 10,
        name: 'clock',
        
        // callbacks
        
        // overwrite functions
        initialize: function() {
            _.extend(this, this.options);
        },
        
        start: function() {
            var context = this;
            $.doTimeout(this.name, this.interval*1000, function() {
                var str = Ion.dateTime(context.format);
                if(context.$el)
                    context.$el.text(str);
                return true;
            });
            $.doTimeout(this.name, true);   //force it to execute once
        },
        
        stop: function() {
           $.doTimeout(this.name);    //cancel timeout
        },
        
        process: function() {
            return false;
        },
        
    });
    
})($);