/**
 * Careboard view module. 
 */
var CareboardView = Ion.View.extend({

    template: 'careboard.html',
    css: 'careboard.css',
    id: 'careboard',

	
    render: function () {
        this.addWidget('clock', new Ion.Widget.Clock({
            el: this.$('#basicinfo #time')[0],
            format: 'dddd, mmmm d,  h:MM TT'
        }));
        var context = this;
        // schedule a timer every 60 seconds.
        $.doTimeout('careboard polling', 120000, function() {
            context._updateData();
			return true;
        }); 
        // delay 1 second and do an one-time update
        $.doTimeout('careboard polling once', 500, function() {
            context._updateData();
            return false;
        }); 
        return this;
    },
	
	uninit: function() {
        $.doTimeout('careboard polling');   // stop the timer
    },
    
    click: function($obj) {
        Ion.log("** " + $obj.attr('id') + ' clicked **');
        return false;
    },
    
    _updateData: function() {
	
//		var tvon = 'true'
 //       if(Ion.platform() == 'ENSEO') {
  //                  var tv = Nimbus.getTVController();
  //                  if (tv) {
   //                     var tvon = tv.getpower();
   //                 }
   //             }
		//Ion.log('Power ' + tvon);
//		if(tvon == 'false')
	//		return true;
        //get data from middleware
        var userData = Ion.fetcher.getUserData();
		var cnt = '';
		// Get Patient MRN
		var url = '', dataobj = {};
		var preferredname = '';
        var mrn = Ion.fetcher.getMRN(true, false);
		
		var main_url = "http://10.54.10.104:9080/ams/aceso/getClinicalData?"

		
		// Get Patient Preferred Name
		url = main_url + "type=patient&mrn=" + mrn+ "&numrec=3&sortorder=asc"
		
		var dataobj = '';
		var xml  = Ion.fetcher.getXdXML(url, dataobj);		
		Ion.log(xml);
		  $(xml).find("item").each(function() {            		  
			preferredname = ($(this).find("value").text());			
          });

		  		
		// Get Estimated Discharge Date
		url = main_url + "type=estimateddisdate&mrn=" + mrn+ "&numrec=1&sortorder=asc"
		var estimateddisdate = '';
		var dataobj = '';
		var xml  = Ion.fetcher.getXdXML(url, dataobj);		
		Ion.log(xml);
		  $(xml).find("item").each(function() {            		  
			var ed = ($(this).find("value").text());			
			if(ed!=''){
			  estimateddisdate = new Date(ed.substr(0,4)+"/"+ed.substr(4,2)+"/"+ed.substr(6,2))
			  estimateddisdate = estimateddisdate.format("mm-dd-yyyy");
			 }
			Ion.log(estimateddisdate);
		});
		  
		// Get diet 
		url = main_url + "type=diet&mrn=" + mrn+ "&numrec=4&sortorder=asc"
		var dataobj = '';
		var dietdesc = '';
		var xml  = Ion.fetcher.getXdXML(url, dataobj);		

		  $(xml).find("item").each(function() {                   
            dietdesc = '<p>' + $(this).find("value").text() + '</p>';           
          });


		// TODO Add code when dietary 
		var diet = '<div class="header">My Diet:</div>';
		if(dietdesc.length >=1) {
			diet = diet + dietdesc;
		} else {		
			diet = diet + '<p>Please discuss your Dietary needs with your care team</p>';
		}

		url = main_url + "type=allergies&mrn=" + mrn+ "&numrec=10&sortorder=asc"
		var dataobj = '';
		var allergies = '';
		var xml  = Ion.fetcher.getXdXML(url, dataobj);		
		cnt = 0;
		 $(xml).find("item").each(function() {            		  
			if (cnt != 0) 
				allergies = allergies + ', ';
			allergies = allergies  + $(this).find("value").text();						
			cnt = cnt + 1; 
          });
		  allergies = allergies + '</p>';
				  
		// TODO Add code when dietary 
		if(allergies.length >=1) {
            diet = diet + '<p>Food Allergies: ' + allergies;
        } else {
			diet = diet + '<p>Food Allergies: Unknown';
		}
		  
		// Get Careteam Data
		url = main_url + "type=careteam&mrn=" + mrn+ "&numrec=200&sortorder=asc"
		var dataobj = '';		
		var enddate = '';
		var careteam = '<div class="header">My Care Team:</div>';
		cnt = 0;
		var xml  = Ion.fetcher.getXdXML(url, dataobj);		
		$(xml).find("item").each(function() {            		  
			if (cnt <=11) {
				desc = ($(this).find("codedescription").text());
				// decided not to show relationship
				//if (desc != 'Attending Physician') 
				//	desc = ''
				value = ($(this).find("value").text());
				enddate = ($(this).find("enddate").text());
				var now = Ion.dateTime("yyyy-mm-dd HH:MM:ss");
				var datecompare = 0;
				if(enddate.length>=1)
					datecompare = Ion.compareDates(enddate,now);				
					Ion.log(datecompare + ' ' + value + ' ' + now + ' ' + enddate);
					if(datecompare >= 0) {
						if(desc!='' && desc.length>=1) {						
							careteam = careteam +  '<p>' + value +  ", " + desc + '</p>'; 	
						} else {
							careteam = careteam +  '<p>' + value + '</p>'; 	
						}
						cnt = cnt + 1;
					}
				}	
			});
		
		// Get Activity		
		url = main_url + "type=activity&mrn=" + mrn+ "&numrec=6&sortorder=asc"
		var dataobj = '';
		var activity = '<div class="header">My Activity:</div>';
		var xml  = Ion.fetcher.getXdXML(url, dataobj);		
		var code = '';
		$(xml).find("item").each(function() {            		  
			desc = ($(this).find("codedescription").text());
			value = ($(this).find("value").text());
			code = ($(this).find("code").text());
			value = value.replace(/\;/g,'<br/>');
			value = value.replace(/\:/g,'<br/>');
			value = value.replace(/\,/g,'<br/>');

			if(code == 'ACES-5937' && (value.length>=1)) 
				value = "Staff Assist - " + value;
			activity = activity +  '<p>' + value + '</p>'; 
			
			cnt = cnt + 1;
        });
		
		// Get Goals
		var url = "http://10.54.10.104:9080/ams/aceso/getClinicalData?"
		url = url + "type=goals&mrn=" + mrn+ "&numrec=1&sortorder=asc"
		var dataobj = '';
		var goals = '<div class="header">My Goals:</div>';
		var xml  = Ion.fetcher.getXdXML(url, dataobj);				
		var cnt = 0;
		$(xml).find("item").each(function() {            		  			
			cnt = cnt + 1;
			desc = ($(this).find("codedescription").text());
			value = ($(this).find("value").text());
			value = value.replace(/\;/g,'<br/>');
			value = value.replace(/\:/g,'<br/>');
			value = value.replace(/\,/g,'<br/>');
			goals = goals +  '<p>' + value + '</p>'; 
        });
		
		
		//Get Vision, Dental, Fall		
		url = main_url + "type=clinical&mrn=" + mrn+ "&numrec=10&sortorder=asc"
		var dataobj = '';
		var clinical = '<div class="header">Clinical:</div>';
		var xml  = Ion.fetcher.getXdXML(url, dataobj);		
		var clinicaldata = '';
		Ion.log(xml);
		$(xml).find("item").each(function() {            		  
			code = ($(this).find("code").text());
			value = ($(this).find("value").text());

			switch (code) {
				case 'ACES-1246':
					if(value<='18')
						clinicaldata = clinicaldata + '<div class="icon" id="skin"></div>';
						break;
				case 'ACES-7583':
					if(value!='NONE' && value != '')
						clinicaldata = clinicaldata + '<div class="icon" id="hearing"></div>';
						break;
				case 'ACES-7584':
					if(value!='NONE' && value != '')
						clinicaldata = clinicaldata + '<div class="icon" id="visual"></div>';
						break;
				case 'ACES-1375':
					if(value!='NONE' && value != '')
						clinicaldata = clinicaldata  + '<div class="icon" id="dentures"></div>';
						break;
				case 'ACES-7481':
					if(value>='3')
						clinicaldata = clinicaldata  + '<div class="icon" id="fall"></div>';
						break;					
				};
		});
			
		// Get Pain		
		url = main_url + "type=pain&mrn=" + mrn+ "&numrec=10&sortorder=asc"
		var dataobj = '';
		var clinical = '<div class="header">Clinical:</div>';
		var xml  = Ion.fetcher.getXdXML(url, dataobj);		
		var pain = '';
		var painscore = '';
		var painscoredate = '';
		
		Ion.log(xml);
		$(xml).find("item").each(function() {            		  
			code = ($(this).find("code").text());
			value = ($(this).find("value").text());
Ion.log(code + ' ' + value);
			switch (code) {
				case 'ACES-7617':					
					pain = value;
					break;
				case 'ACES-7615':					
					pain = value;
					break;
				case 'ACES-5695':										
					painscore = value;
					painscoredate = ($(this).find("startdate").text());			
					painscoredate = painscoredate.substr(0,16);
					break;
				case 'ACES-5690':					
					painscore = value;
					painscoredate = ($(this).find("startdate").text());			
					painscoredate = painscoredate.substr(0,16);
					break;		
				case 'ACES-1268':					
					painscore = value;
					painscoredate = ($(this).find("startdate").text());			
					painscoredate = painscoredate.substr(0,16);
					break;		
			};
		 });

		// Put Pain and other clinical data together
		if (pain!='') {
			var pos = pain.indexOf("(");			
			if(pos >= 1) 
				pain = pain.substr(0,pos-1);
				clinical = clinical + '<p>' + painscore + " - " + pain + " - " + painscoredate + '</p>' + '<div id="icons">' + clinicaldata;			
			} else {
				clinical = clinical + '<div id="icons">' + clinicaldata;
			}
		

		// Get Support Person
		var mysupport = '<div class="header">My Support Person:</div>';
		url = main_url + "type=mysupport&mrn=" + mrn+ "&numrec=3&sortorder=asc"
		
		var dataobj = '';
		var xml  = Ion.fetcher.getXdXML(url, dataobj);		
		
		var mysupportperson = '';
		Ion.log('typof xml'+ typeof xml.length);
		  $(xml).find("item").each(function() {            		  
			mysupportperson = unescape($(this).find("value").text()); 
          });
		  Ion.log(mysupportperson);
			if(mysupportperson.length >=1) 
				mysupport = mysupport + '<p>'+mysupportperson+'</p>'
			else
				mysupport = mysupport + '<p>Please enter your support person on your bedside terminal(MyLink)</p>';
				
		  
		
		
		if(preferredname) {
			this.$('#basicinfo #name').text('"' + preferredname + '"');
		} else { 
			this.$('#basicinfo #name').text(userData.patient.userFullName);
		}
		
		
			this.$('#dischargedate').html('<div class="header2">Estimated Discharge:</div><p> ' + estimateddisdate + '</p>');
		
	
		var room = userData.home.roomNumber.toUpperCase();
        this.$('#basicinfo #room').text(room);
        this.$('#basicinfo #phone').text(userData.home.phoneNumber);
        this.$('#careteam').html(careteam);
        this.$('#activity').html(activity);
        this.$('#goals').html(goals);
		this.$('#clinical').html(clinical);
		this.$('#diet').html(diet);
		this.$('#supportperson').html(mysupport);
	
	}
	

	
	
});