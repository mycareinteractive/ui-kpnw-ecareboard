/**
 * Discharged view module. 
 */
var DischargedView = Ion.View.extend({

    template: 'discharged.html',
    css: 'discharged.css',
    id: 'discharged',
	
    render: function () {	
		var context = this;
		this.addWidget('clock', new Ion.Widget.Clock({
            el: this.$('#time')[0],
            format: 'dddd, mmmm d  h:MM TT'
        }));
		
		var userData = Ion.fetcher.getUserData(true);
		this.$('#info #room').text(userData.home.roomNumber);
        this.$('#info #phone').text(userData.home.phoneNumber);

    },
});
