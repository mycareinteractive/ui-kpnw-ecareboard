var Ion = Ion || {};

(function ($) {
    
    /**
     * The navigator is a singleton that manages the boot process and page transitions 
     */
    Ion.navigator = {
        
        // public properties
        
        /**
         * The current main content view 
         */
        currentView: null,
        
        /**
         * The optional global dock/launch bar view. 
         */
        dockView: null,
        
        // private properties
        
        // public functions
        
        /**
         * Start the navigator. This function will kick off the boot process. 
         */
        start: function () {
            this._initialize();

            if(!Ion.fetcher.getPortalConfigData()) {
                // No config data.
                this.reload(60 * 1000, 'Failed to initialize app.');
                return;
            }
            
            // start event loop
            Ion.dispatcher.initialize();
            
            // set initial URL
            if(!Ion.fetcher.getUserData() || Ion.fetcher.getUserData().home.status!='1') {
                // discharged, skip main page directly
                location.hash = '#discharged';
            }
            else {
                // load main page
                location.hash = '';
            }
            
            // start history tracking
            Backbone.history.start({
                hashChange: Ion.hashChange(),
                root: Ion.rootUrl()
            });
            
            this._startPollers();
            
            $('div#loading').hide();
        },
        
        /**
         * Restart the app. On ENSEO platform this also triggers browser reset. 
         */
        reload: function (delay, message) {
            $('div#loading').show();
            if(!delay || typeof delay != 'number' || delay < 1000) {
                if(Ion.platform() == "ENSEO") {
                    Nimbus.reload(true);
                }
                location.href = '/ewf/';
            }
            else {
                if(message)
                    Ion.navigator.progress(message + ' Reload app in ' + delay/1000 + ' seonds...');
                setTimeout(function(){
                    Ion.navigator.reload(delay-1000, message);                    
                }, 1000);
            }
        },
        
        progress: function (message, logging) {
            $('div#loadinginfo').html('<p>' + message + '</p>');
            if(logging)
                Ion.log(message);
        },
        
        debugInfo: function (status) {
            var debug = $('div#debuginfo');
            var str = '<h1>Info</h1><ul>';
            str += '<li><h2>Core version</h2><p>' + Ion.APP_VERSION + '</p></li>';
            str += '<li><h2>Platform</h2><p>' + Ion.platform() + '</p></li>';
            debug.html(str);
            debug.show().delay(10000).hide('fast');
        },
        
        display: function (view, data, options) {
            var context = this;
            if(this.currentView) { 
                this.currentView.destroy(); 
            }
            
            this.currentView = view;
            
            this.currentView.show(data, function(){
                $("div#content div#current").html(context.currentView.el);    
            });
        },
        
        authenticate: function() {
            var userData = Ion.fetcher.getUserData(true);
            
            if(!Ion._global.online) { // server not available
                Ion._global.offlineCount++;
                if(Ion._global.offlineCount>=5) {
                    this.reload(60 * 1000, 'Failed to authenticate user.');
                    return false;
                }
                return true;
            }
            
            Ion._global.offlineCount = 0;
            
            if(!userData.home) {
                // Not provisioned, go to provision page.
                location.href = '/ewf/';
            }
            
            if(userData.home.status != '1') {
                // Discharged or vacant room/bed, go to discharged page.
                Ion.router.navigate('discharged', { trigger: true, replace: true });
            }
            if(userData.home.status == '1' && Backbone.history.fragment == 'discharged') {
                // admission detected, reload
                Ion.navigator.reload();
            }
            return true;
        },
        
        // initialize the app
        _initialize: function () {
            Ion.deviceID();
            
            if (Ion.platform() == 'ENSEO') {
                // make hash change detection really slow for performance reason   
                Backbone.history.interval = 30000;
                Nimbus.setLogLevel("Nimbus",2);
                Nimbus.setLogLevel("Lib",2);
                Nimbus.setHandshakeTimeout(0);
                Nimbus.setTimeZoneMode('Name');
                Nimbus.setTimeZoneName(Ion.APP_TIMEZONE, Ion.APP_TIMEZONE_NAME);
                Nimbus.setNetworkAPIEnable(true);           // Turn on NetAPI
                Nimbus.setNetworkAPIAllCallsEnable(true);   // Enable all NetAPI calls
                Nimbus.setAutoSpacialNavigationEnable(false);   // disable Opera keyboard nav
				// Set to false rather than comment it, so all boxes shut off the update
				Nimbus.setIPAutoUpdateParameters(false,'239.255.11.103','8000');
                // Enable console and telnet debugging
				Nimbus.setConsoleEnabled(true, true);
				Nimbus.setTelnetEnabled(true, true);
                var TVController = Nimbus.getTVController();
                if (TVController) {
                    TVController.setVolume(10);
                }
                Ion.handshake();
                setInterval("Ion.handshake()", 30000);
            }
            else if (Ion.platform() == 'NEBULA') {
            }
            else if (Ion.platform() == 'TCM') {
            }
            else {
            }
            
            // App root URL
            Ion._global.rootUrl = location.pathname.substring(0, location.pathname.lastIndexOf("/") + 1);
            
            // User data and config data, synchronous calls
            this.progress('Authenticating...', true);
            this.authenticate();
              
            this.progress('Loading portal config data...', true);
            Ion.fetcher.getPortalConfigData(true);
            
            if(Ion.platform() != 'DEV') {
                this.progress('Reporting hardware info...', true);
                Ion.fetcher.sendPlatformInfoData(true, true); // async
            }
            return true;
        },
        
        // setup scheduled jobs such as auto-reload every so often, check for discharge...etc 
        _startPollers: function() {
            var interval = 60000;
            var context = this;
            
            var config = Ion.fetcher.getPortalConfigData();

            // polling for user data and discharge
            interval = config.checkUserDataInterval || 600000;
            var userDataRetry = 0;
            $.doTimeout('userdata polling', interval, function() {
                return Ion.navigator.authenticate();
            });
            
            // auto sleep and reload poller
            interval = 120*1000;
            $.doTimeout('auto sleep and reload', interval, function() {
                context._autoSleepAndReload();
                return true;
            });
        },
        
        // check for auto sleep function
        _autoSleepAndReload: function() {
            var config = Ion.fetcher.getPortalConfigData();
            
            var now = new Date();
            var hour = now.getHours();
            var minute = now.getMinutes();
            
            var from = config.autoSleepFrom || '22:00';
            var fromHour = from.split(':')[0];
            var fromMinute = from.split(':')[1];
            
            var to = config.autoSleepTo || '6:00';
            var toHour = to.split(':')[0];
            var toMinute = to.split(':')[1];
            
            var reload = config.autoReloadAt || '3:00';
            var reloadHour = reload.split(':')[0];
            var reloadMinute = reload.split(':')[1];
            
            // reload
            if(config.autoReload && config.autoReload!=false && config.autoReload!='false') {
                if(hour==reloadHour && (minute-reloadMinute) >=0 && (minute-reloadMinute) <= 2) {
                    Ion.log('***********************************************');
                    Ion.log('Auto reload time met, reloading...');
                    Ion.log('***********************************************');
                    Ion.navigator.reload();
                }
            }
            
            
            // auto sleep
            if(config.autoSleep && config.autoSleep!=false && config.autoSleep!='false') {
            
                var state = null;
                
                // We only auto-turn on/off TV within 2 minute window of the sleep/wakeup time.
                // If someone forces the TV on/off after 2 minute past sleep/wakeup time, they can overwrite the feature.
                if(hour==fromHour && (minute-fromMinute) >=0 && (minute-fromMinute) <= 2) {
                    Ion.log('***********************************************');
                    Ion.log('Auto sleep time is met, turning TV off...');
                    Ion.log('***********************************************');
                    //state = false;	// nurses want them to stay on
                }
                else if(hour==toHour && (minute-toMinute) >=0 && (minute-toMinute) <= 2){
                    Ion.log('***********************************************');
                    Ion.log('Auto wakeup time is met, turning TV on...');
                    Ion.log('***********************************************');
                    state = true;
                }
                
                if(Ion.platform() == 'ENSEO') {
                    var tv = Nimbus.getTVController();
                    if (tv && state!=null) {
                        power = tv.setPower(state);
                    }
                }
            }
        }

        
        
    }; // Ion.navigator

   
     
    
})($);
