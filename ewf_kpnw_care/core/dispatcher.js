var Ion = Ion || {};

(function ($) {
    
    // event types
    TYPE_INVALID = 'INVALID';
    TYPE_COMMAND = 'COMMAND';
    TYPE_NOTIFICATION = 'NOTIFICATION';
    TYPE_MOUSE = 'MOUSE';
        
    Ion.dispatcher = _.extend({}, Backbone.Events, {
        
        initialize: function() {
            var context = this;
            // register browser key events
            $(document).keydown(function(e){
                return context._onBrowserKeydown(e);
            });
            
            if (Ion.platform() != 'ENSEO') {
                // mouse click events
                $('div#content').on('click', 'a, div[data-role="button"]', function(e){
                    return context._onBrowserClick(e); 
                });
            }
            
            // register Enseo events
            if(Ion.platform() == 'ENSEO') {
                Nimbus.addEventListener(function(e){
                    return context._onEnseoEvent(e);
                });                
            }
        },
    
        eventHandler: function(type, msg, data) {
            try {
                //Ion.log('Dispatching event ' + type + ' | ' + msg + ' | ' + data);
                var handled = false;
                
                // send to current view
                if(Ion.navigator.currentView) {
                    handled = Ion.navigator.currentView.process(type, msg, data);
                }
                // try dock
                if(!handled && Ion.navigator.dockView) {
                    handled = Ion.navigator.docView.process(type, msg, data);
                }
                
                return handled;
            }
            catch (err) {
                Ion.error('Error handling event "' + type + '"    ' + err);
                return false;    
            }
        },
        
        _onBrowserKeydown: function(e) {
            Ion.log('EVENT | BROWSER | Keydown,' + e.keyCode);
            var keyName = this._getKeyName(e.keyCode);
            if(!keyName) {
                Ion.log('Key ' + e.keyCode + 'ignored');
                return false;
            }
            var handled = this.eventHandler(TYPE_COMMAND, keyName);
            if(handled)
                e.preventDefault();
            return handled;
        },
        
        _onBrowserClick: function(e) {
            var target = e.currentTarget;
            var name = target.localName + ' id=' + target.id + ' class=' + target.className
            Ion.log('EVENT | BROWSER | Click,' + name);
            var handled = this.eventHandler(TYPE_MOUSE, target);
            if(handled)
                e.preventDefault();
            return handled;
        },
        
        _onEnseoEvent: function(ev) {
            Ion.log('EVENT | ENSEO | ' + ev.EventType + ',' + ev.EventMsg + ',' + ev.EventData);
            var typemap = [TYPE_INVALID, TYPE_COMMAND, TYPE_NOTIFICATION];

            var keyName = ev.EventMsg;
            if(ev.EventType == Nimbus.Event.TYPE_COMMAND) {
                var Cmd = Nimbus.parseCommand(ev.EventMsg);
                keyName = this._getKeyName(Cmd.Code);
                if(!keyName) {
                    Ion.log('Key ' + Cmd.Code + 'ignored');
                    return false;
                }
                
            }
                
            var handled = this.eventHandler(typemap[ev.EventType], keyName, ev.EventData);
            Ion.log('Command key: ' + ev.EventMsg + (handled?' processed':' not processed'));
            return handled;
        },
        
        _getKeyName: function(keycode) {
            // button commands
            var keys = Array();
            keys[8]  = 'BACK';
            keys[13] = 'ENTER';
            keys[35] = 'END';
            keys[36] = 'HOME';
            keys[37] = 'LEFT';
            keys[38] = 'UP';
            keys[39] = 'RIGHT';
            keys[40] = 'DOWN';
            keys[33] = 'CHDN';
            keys[34] = 'CHUP';
            
            keys[61447] = 'MENU';   // enseo menu key
            keys[216]   = 'MENU';   // tcm remote menu
            keys[17]    = 'MENU';   // ctrl
            
            keys[48] = '0';
            keys[49] = '1';
            keys[50] = '2';
            keys[51] = '3';
            keys[52] = '4';
            keys[53] = '5';
            keys[54] = '6';
            keys[55] = '7';
            keys[56] = '8';
            keys[57] = '9'; 
        
            keys[61446] = 'ENTER';  // enseo enter
            keys[61444] = 'LEFT';   // enseo left
            keys[61442] = 'UP';     // enseo up
            keys[61445] = 'RIGHT';  // enseo right
            keys[61443] = 'DOWN';   // enseo down
        
            keys[61441] = 'POWR';   // power
            keys[61507] = 'POWR';   // power
            keys[61508] = 'POWR';   // power
            keys[27]    = 'POWR';   // esc
            keys[46]    = 'POWR';   // del
            keys[61521] = 'CC';     // closed caption
            keys[67]    = 'CC' ;    // c
            keys[61483] = 'CHUP';   // channel+
            keys[61484] = 'CHDN';   // channel-
            keys[61449] = 'VOLU';   // volume+
            keys[61448] = 'VOLD';   // volume-
            keys[187]   = 'VOLU';   // +
            keys[189]   = 'VOLD';   // -
            keys[107]   = 'VOLU';   // + (numpad)
            keys[109]   = 'VOLD';   // - (numpad)
        
            keys[61464] = 'PLAY';   // pause
            keys[61465] = 'STOP';   // stop
            keys[61464] = 'PAUS';   // pause
            keys[61467] = 'RWND';   // rewind
            keys[61468] = 'FFWD';   // fast forward     
        
            //for trick play testing
            keys[186]   = 'PLAY';   // ;    
            keys[222]   = 'STOP';   // '
            keys[191]   = 'PAUS';   // /
            keys[188]   = 'RWND';   // ,
            keys[190]   = 'FFWD';   // .
            
            return keys[keycode];
        }
    });
})($);