var Ion = Ion || {};

(function ($) {
    
    /**
     * Base class for Ion View.
     */
    Ion.View = Backbone.View.extend({
        
        // public properties
        
        id: 'ionview',  //eg: secondary
        
        tagName: 'div', //eg: div
        
        className: null,//eg: aboutus
        
        template: null, //eg: secondary.html
        
        css: null,      //eg: secondary.css
        
        // private properties
        widgets: {},
        
        isActive: false,
        
        // overwrite functions
        
        /**
         * Overwrite to put constructor code. 
         */
        init: function () {
        },

        /**
         * Overwrite to put destructor code. 
         */
        uninit: function () {
        },
        
        /**
         * Overwrite to include render content logic. 
         */
        render: function () {
        },
        
        blur: function($obj) {
            return false;
        },
        
        focus: function($obj) {
            return false;
        },
        
        click: function($obj) {
            return false;
        },
        
        /**
         * Not recommended for overwriting unless you want completely change how render works 
         */
        show: function(data, success) {
            var context = this;
            if(this.css) {
                $("<link/>", {rel: "stylesheet", type: "text/css", href: "css/"+this.css}).appendTo("head");
            }
            if(this.template) {
                this.$el.load('templates/' + this.template + ' #view', function(responseText, textStatus, XMLHttpRequest) {
                    context.render();
                    context.isActive = true;
                    if(success) 
                        success();
                });
            }
        },
        
        /**
         * Custom event handler.  Overwrite to include your logic, such as key strock handling for POWER, MENU, TRICK PLAY...;
         *   or logic when getting player notification or tcp data channel. 
         * Note if you decide to overwrite the process() function, make sure to call parent's process() function at the end.
         *      XXXXXX.__super__.process.call(this, type, msg, data);
         * where XXXXXX is your View class.
         * This ensures that widgets, special keys (MENU, POWER) are handled properly.
         * @param {String} type 'COMMAND', 'NOTIFICATION' or 'MOUSE'.
         * @param {String} msg  The command keyCode, the notification subject or mouse click selector.
         * @param {Object} data Optional data. Primarilly used in notification. 
         */
        process: function(type, msg, data) {
            if(!this._validateEvent('NOTIFICATION', msg, data)) {
                return false;
            }
            
            var handled = false;
            // MENU, HOME, POWER keys
            var keys = ['MENU', 'HOME']
            if(type == 'COMMAND' && keys.indexOf(msg) >= 0) {
                // go to default route
                Ion.router.navigate("", {trigger: true});
                return true;
            }
            else if(type == 'MOUSE') {
                var $obj = $(msg);
                if($obj.length > 0) {
                    this.blur();
                    this.focus($obj);
                    return this.click($obj);
                }
            }
            
            // proces widgets
            for(var name in this.widgets) {
                var widget = this.widgets[name];
                if(widget && widget.process)
                    handled = widget.process(type, msg, data);
                if(handled)
                    break;
            }
            return handled;
        },
        
        // public functions
        initialize: function () {
            this.on('COMMAND', this._commandHandler, this);
            this.on('NOTIFICATION', this._notificationHandler, this);
            this.on('MOUSE', this._mouseHandler, this);
            this.init();
        },
        
        destroy: function() {
            this.uninit();
            if(this.css) {
                $('link[href="css/' + this.css + '"]').remove();
            }
            
            this.isActive = false;
            
            for(var name in this.widgets) {
                var widget = this.widgets[name];
                if(widget && widget.stop)
                    handled = widget.stop();
            }
            this.widgets = {};
            
            if(this.beforeDestroy) { 
                this.beforeDestroy(); 
            }
            
            this.unbind();
            this.remove();
            
            delete this.$el; // Delete the jQuery wrapped object variable
            delete this.el; // Delete the variable reference to this node
        },
        
        addWidget: function(name, widget) {
            if(name) {
                if(this.widgets[name]) delete this.widgets[name];
                this.widgets[name] = widget;
                if(widget && widget.start)
                    widget.start();
            }
        },
        
        removeWidget: function(name) {
            if(name) {
                var widget = this.widgets[name];
                if(widget && widget.stop)
                    widget.stop();
                delete this.widgets[name];
            }
        },
        
        getWidget: function(name) {
            if(name && this.widgets[name]) {
                return this.widgets[name];
            }
            return null;
        },
        
        // private functions
        _validateEvent: function(msg, data) {
            if(!this.isActive) {
                Ion.log('View "' + this.id + '" is not active yet, ignore event');
                return false;
            }
            return true;
        }
    });
    
})($);
